from os import path

mrt = []
with open(path.join('.', 'in.txt'), 'r') as f:
    for i in f.readlines():
        mrt.append(i.strip())

# (1) list 共有多少元素?
print('1.', len(mrt))
print()

# (2) 某個元素是否在 list 中?
print('2.', '新莊線 OR20 丹鳳' + ('' if '新莊線 OR20 丹鳳' in mrt else '不') + '在 list 中')
print('2.', '新莊線 OR 鳳' + ('' if '新莊線 OR2 鳳' in mrt else '不') + '在 list 中')
print()

# (3) 從 list 中依序取出所有元素, list元素不刪除.
print('3.')
[print(i) for i in mrt]
print()

# (4) 從 list 前方加入一個元素.
mrt.insert(0, 'Hello')
print('4.', mrt)
print()

# (5) 從 list 後方加入一個元素.
mrt.append('Hello')
print('5.', mrt)
print()

# (6) 從 list 任意位置加入一個元素.
mrt.insert(3, 'Add')
print('6.', '新增一個元素在 index 3')
print('-' * 30)
print(mrt)
print()

# (7) 從 list 任一位址取出一個元素, list 元素不刪除.
print('7.', 'Item at index 3, item is "%s"' % mrt[3])
print()

# (8) 從 list 任一位址取出前10個元素, list 元素不刪除.
print('8.', mrt[:10])
print()

# (9) 從 list 任一位址取出後10個元素, list 元素不刪除.
print('9.', mrt[-10:])
print()

# (10) 從 list 任一位址取出後奇數元素, list 元素不刪除.
print('10.', mrt[::2])
print()

# (11) 從 list 任一位址取出一個元素, list 元素刪除.
print('11.', mrt.pop(0), 'has been take and remove')
print('-' * 30)
print(mrt)
print()

# (12) list 元素順序反轉.
ll = [1, 2, 3, 4, 5]
print('12.')
print('List:', ll)
ll.reverse()
print('After reverse:', ll)
print()

# (13) list 元素順序排序.
ll = [4, 2, 5, 6, 7, 1]
print('13.')
print('List:', ll)
ll.sort()
print('After sort:', ll)
print()

# (14) 某個元素在 list 中出現幾次?
ll = [1, 1, 2, 3, 4, 5]
print('14.')
print('List:', ll)
print('There is {} "1" in list'.format(ll.count(1)))

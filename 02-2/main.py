from os import path

with open(path.join('.', 'in.txt'), 'r') as f:
    data = f.read()
    data = data.strip()

# (1) 共用了多少字?
print('1.', len(data))
print()

# (2) 用了多少個"我"?
print('2.', data.count('我'))
print()

# (3) 用了多少個"我們"?
print('3.', data.count('我們'))

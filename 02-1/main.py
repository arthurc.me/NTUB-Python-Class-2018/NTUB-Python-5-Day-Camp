from os import path

with open(path.join('.', 'in.txt'), 'r') as f:
    data = f.read()
    data = data.strip()

target = data.split(' ')

# (1) 取出前 100 個字.
print('1.', target[:100])
print()

# (2) 取出最後 100 個字.
print('2.', target[-100:])
print()

# (3) 取出所有奇數文字.
print('3. ', target[::2])
print()

# (4) 第 1 個 z 出現在第幾個字?
r = ''
for i, c in enumerate(target):
    if 'z' in c:
        r = i
        break

print('4.', r if r else 'Not exit')
print()

# (5) 反轉字串.
print('5.', list(reversed(target)))
print()

# (6) 轉成全部大寫.
print('6.', data.upper())
print()

# (7) 轉成全部小寫.
print('7.', data.lower())
print()

# (8) 共有多少逗號?
print('8.', data.count(','))
print()

# (9) 清除所有標點符號.
t = ',.-;()[]-\'"'
d = data
for i in t:
    d = d.replace(i, '')

print('9.', d)
print()

# (10) 用了那些單字 (可重覆)?
print('10.', target)
print()

# (11) 用了那些單字 (不重覆)?
print('11. ', list(set(target)))
print()

# (12) 每個使用的單字在文章中出現幾次?
print('12.')
for word in set(target):
    print(word, target.count(word))

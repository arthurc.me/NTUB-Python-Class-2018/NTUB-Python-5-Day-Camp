from os import path

mrt = []
with open(path.join('.', 'in.txt'), 'r') as f:
    for i in f.readlines():
        mrt.append(tuple(i.strip().split(' ')))

# (1) 試試能否修改 tuple 的內容?
print('1.', end=' ')
try:
    mrt[0][0] = 100
except TypeError:
    print('Can\'t change value in tuple.')

print()


# (2) 只印出 '土城線' 的內容.
result = [' '.join(i) for i in mrt if '土城線' == i[0]]
print('2.', result)
print()

# (3) 將所有'站名'(不包括 '路線名' 及' 代號')加入一個 list 中.
s = [i[2] for i in mrt]
print('3.', s)

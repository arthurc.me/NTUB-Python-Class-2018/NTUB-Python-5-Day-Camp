import numpy as np

from os import path

from matplotlib import pyplot as plt


PWD = path.dirname(path.abspath(__file__))


def main():
    data = np.genfromtxt(path.join(PWD, 'iris.csv'), delimiter=',')

    fig = plt.figure()
    sub = fig.add_subplot(111)

    data1 = data[data[:, 4] == 1]
    data2 = data[data[:, 4] == 2]
    data3 = data[data[:, 4] == 3]

    sub.plot(data1[:, 0], data1[:, 1], 'bd', alpha=0.2)
    sub.plot(data2[:, 0], data2[:, 1], 'rd', alpha=0.2)
    sub.plot(data3[:, 0], data3[:, 1], 'gd', alpha=0.2)

    plt.title('Iris Flower')
    plt.legend(['setosa', 'versicolor', 'virginica'], numpoints=1, loc='upper left')
    plt.grid(True)
    plt.show()


if __name__ == '__main__':
    main()

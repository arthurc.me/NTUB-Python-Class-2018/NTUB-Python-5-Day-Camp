import numpy as np

from os import path

from sklearn.neighbors import KNeighborsClassifier


PWD = path.dirname(path.abspath(__file__))


def main():
    data = np.genfromtxt(path.join(PWD, 'iris.csv'), delimiter=',')

    np.random.shuffle(data)

    training, testing = data[:120], data[120:]

    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(training[:, :4], training[:, 4])

    ans = testing[:, 4]
    predicted = knn.predict(testing[:, :4])

    result = []
    for a, p in zip(ans, predicted):
        r = a == p
        result.append(r)

        print('預期結果：{}'.format(a))
        print('猜測結果：{}'.format(p))
        print('猜測結果：{}'.format('正確' if r else '錯誤'))
        print('-'*30)

    print('總結果：{}/{}'.format(result.count(True), len(result)))
    print('正確率：{}'.format(np.sum(result) / len(result)))


if __name__ == '__main__':
    main()

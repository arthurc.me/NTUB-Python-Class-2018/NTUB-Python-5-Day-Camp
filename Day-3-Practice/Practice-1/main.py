import jieba

from os import path


PWD = path.dirname(path.abspath(__file__))

data = {}


def init():
    global data

    with open(path.join(PWD, 'sentimentWords.csv'), 'r', encoding='utf-8') as f:
        f.readline()
        for line in f.readlines():
            line = line.strip()
            line = line.split(',')
            data[line[1]] = line[2]


def sentiment(doc):
    global data

    if not data:
        init()

    result = []
    result_value = []
    cut_result = jieba.cut(doc)
    for word in cut_result:
        if word not in data:
            continue

        result.append(word)
        result_value.append(float(data[word]))

    score = sum(map(lambda x: x - 5, result_value)) / len(result_value)
    return score, result, result_value


def main():
    with open(path.join(PWD, 'in.txt'), 'r', encoding='utf-8') as f:
        doc = f.read()
        doc = doc.strip()

    score, result, result_value = sentiment(doc)
    print(score)
    print(result)
    print(result_value)


if __name__ == '__main__':
    main()

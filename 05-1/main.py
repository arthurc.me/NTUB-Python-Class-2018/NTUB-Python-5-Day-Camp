import random

from os import path

mrt = []
with open(path.join('.', 'in.txt'), 'r') as f:
    for i in f.readlines():
        mrt.append(tuple(i.strip().split(' ')))

# !!! 使用 range !!!

# (1) 印出 list 中的前半段內容?
r = [mrt[i] for i in range(len(mrt) // 2)]
print('1.', r)
print()

# (2) 印出 list 中的後半段內容?
r = [mrt[i] for i in range(len(mrt) // 2, len(mrt))]
print('2.', r)
print()

# (3) 印出 list 中的奇數內容?
r = [mrt[i] for i in range(0, len(mrt), 2)]
print('3.', r)
print()

# (4) 印出 list 中的偶奇數內容?
r = [mrt[i] for i in range(1, len(mrt), 2)]
print('4.', r)
print()

# (5) 印出 list 中亂數挑選的一半內容?
print('5.', random.sample(mrt, len(mrt) // 2))
